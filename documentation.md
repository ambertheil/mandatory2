---
layout: default
title: Documentation
---
### IDE - VS code
   We are working with Visual Studio Code as our IDE which is free code editor made by Microsoft for Windows, Linux and macOS.

### Create enviroment
   Create a virtual enviroment with python on your computer by following the next commands: 

   - python3 -m venv venv
   - . venv/bin/activate

   You can call you virtual enviroment whatever you want, in our case we call it venv.

### Install dependencies
   Make sure you have the latest enviroment has the python 3.7 installed.
   Once you are in the terminal you need to run following commands:

   - cd _bankapp
   - python --version
   - pip install -r requirements.txt

### Run the project 
   To run the project, you have to make sure that you have activated your enviroment and that you are in the right folder "_bank_app". 
   Then run following commands:

   - python manage.py runserver

### Coding standards 
   We used camelCase for functions and classes, snake_case for fields and variables. We decided to use snake_case because Pylint (our linting plugin) recommended us to do so while working locally. 

### Use linting
   Make sure you follow our codeing standards. To check you code run following command: 

   - pylint **/*.py

   If you want to make changes to the codeing standards, checkout the .pylinrc file. 

### Use branches on gitlab
   When you need to make changes to the project, create a new branch and make you workflow inthere.

   - git branch BRANCHNAME
   - git checkout BRANCHNAME
   - Make changes in your code
   - git status
   - git add .
   - git commit -m "YOUR MESSAGE"
   - git push --set-upstream origin BRANCHNAME

It is very importent that you check if the pipeline succeeded, then create a merge request and wait for it to be merged to the Master branch. You can do it yourself if you have the privilliges or else you must wait for someone with the right privilliges to do it.


