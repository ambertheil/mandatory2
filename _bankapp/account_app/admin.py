from django.contrib import admin
from .models import Account, Account_Transaction

admin.site.register(Account)
admin.site.register(Account_Transaction)

# Register your models here.
