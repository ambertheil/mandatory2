from django.test import TestCase
from django.contrib.auth.models import User

from .models import Account, Account_Transaction
# Create your tests here.


class TransactionTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user
        testuser1 = User.objects.create_user(
            username='testuser1', email="test@test.com", password='abc123')
        testuser1.save()

        # Create a account
        test_account = Account.objects.create(
            user=testuser1, text="test_account", number=123, balance=999)
        test_account.save()

        # Createa a transaction
        test_transaction = Account_Transaction.objects.create(
            user=testuser1, number=123, balance=123, transaction_id=4, transaction_type="Initial Deposit", transaction_amount=12)
        test_transaction.save()

    def testUserCreated(self):
        user = User.objects.get(username="testuser1")
        expected_username = f'{user.username}'
        expected_email = f'{user.email}'

        self.assertEqual(expected_username, "testuser1")
        self.assertEqual(expected_email, "test@test.com")

    def testAccountCreated(self):
        # Get testuser
        user = User.objects.get(username="testuser1")
        # Getting matching account

        user_account = Account.objects.get(user=user)
        expected_account_username = f'{user_account.user.username}'
        expected_account_number = f'{user_account.number}'

        self.assertEqual(expected_account_username, "testuser1")
        self.assertEqual(expected_account_number, "123")

    def testTransactionCreated(self):
        # Get testuser
        user = User.objects.get(username="testuser1")
        # Getting matching account
        user_account = Account.objects.get(user=user)
        # Getting matchin transaction
        account_transaction = Account_Transaction.objects.get(user=user)

        expected_account_transaction_username = f'{account_transaction.user.username}'
        expected_account_transaction_id = f'{account_transaction.transaction_id}'

        self.assertEqual(expected_account_transaction_username, "testuser1")
        self.assertEqual(expected_account_transaction_id, "4")
