import random
from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)
    number = models.IntegerField(default=0)
    balance = models.IntegerField(default=0)

    @classmethod
    def create(cls, user, text, number, balance):
        account = cls()
        account.user = user
        account.text = text
        account.number = number
        account.balance = balance
        account.save()

    @classmethod
    def withdraw(cls, number, amount):
        transaction_id = random.randint(100000, 400000)
        account = Account.objects.get(number=number)
        account.balance = account.balance - amount
        account.save()

        # CREATE account TRANSACTION WITH NEW TRANSACTION ID
        Account_Transaction.create(account.user, account.number,
                                   account.balance, transaction_id, "withdraw", amount)

    @classmethod
    def deposit(cls, number, amount):
        transaction_id = random.randint(100000, 400000)
        account = Account.objects.get(number=number)
        account.balance = account.balance + amount
        account.save()

        # CREATE account TRANSACTION WITH NEW TRANSACTION ID
        Account_Transaction.create(account.user, account.number,
                                   account.balance, transaction_id, "deposit", amount)

    def toggle_status(self):
        # self.text = not self.text
        self.save()

    def __str__(self):
        return f"Account: {self.number} User: {self.user}"


class Account_Transaction(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    number = models.IntegerField(null=False)
    balance = models.IntegerField(null=False)
    transaction_id = models.IntegerField(default=0)
    transaction_type = models.CharField(default="Not Stated", max_length=250)
    transaction_amount = models.IntegerField(default=0, null=False)

    @classmethod
    def create(cls, user, number, balance, transaction_id, transaction_type, transaction_amount):
        account_transaction = cls()
        account_transaction.user = user
        account_transaction.number = number
        account_transaction.balance = balance
        account_transaction.transaction_id = transaction_id
        account_transaction.transaction_type = transaction_type
        account_transaction.transaction_amount = transaction_amount
        account_transaction.save()

    def __str__(self):
        return f"Transaction ID: {self.transaction_id} Account Number: {self.number} User: {self.user}"
