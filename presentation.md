---
layout: default
title: Presentation
---
### Programming language & Framework

  - Django and python
  - SQlite database

### CI/CD pipeline 
  
  - Tests
  - Linting
  - Deployment

### Coding standard

  - Pylint
  - Link to .pylintrc file 

### Tests for our project

  - UserTest, AccountTest, TransactionTest

### Documentation

  - [Documentation](https://ambertheil.gitlab.io/mandatory2/documentation/)

## Version Control System

  - Git repository
  - Git messages 
  - Branches 
